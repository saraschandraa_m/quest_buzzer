package socketconnectons;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.smack.questbuzzer.DeviceDetailFragment;
import com.smack.questbuzzer.DeviceListFragment;
import com.smack.questbuzzer.R;
import com.smack.questbuzzer.WiFiDirectActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by KMac on 8/11/16.
 */
public class ClientSocketConnection {
    private static final ClientSocketConnection INSTANCE = new ClientSocketConnection();
    //    private Context mContext;
    private View mConvertView;

    private FragmentManager fm;
    private boolean isRunning;

    public static ClientSocketConnection getInstance() {
        return INSTANCE;
    }

    public void initConnection(FragmentManager fm, String host, View mConvertView) {

        this.fm = fm;
        new Thread(new SocketInitialization(host)).start();
        this.mConvertView = mConvertView;
        isRunning = true;

    }

    private class SocketInitialization implements Runnable {
        String host;

        public SocketInitialization(String host) {
            this.host = host;
        }

        @Override
        public void run() {

            try {
                Socket socket = new Socket();
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, 7777)), 5000);
                Sockets.getInstance().putClientSocket(socket);

                sendMessage(WiFiDirectActivity.deviceName);

                new Thread(new ReceiveMessage()).start();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private class ReceiveMessage implements Runnable {
        String[] servertText;
        String blockMsg, teamName;

        @Override
        public void run() {
            try {
                Socket socket = Sockets.getInstance().getClientSocket();

                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                final String textFromClient = in.readLine();

                if (textFromClient != null) {

                    final String arrConnectText[] = textFromClient.split("<SPLIT>");
                    if (arrConnectText.length == 2) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                DeviceListFragment fragment = (DeviceListFragment) fm
                                        .findFragmentById(R.id.fragList);
                                if (arrConnectText[0].equalsIgnoreCase("connect"))
                                    fragment.updateConnectDevice(arrConnectText[1]);

                            }
                        });
                    } else {

                        Log.d("", "ssuab text from server in client: " + textFromClient);
                        servertText = textFromClient.split(",");
                        blockMsg = servertText[0];
                        teamName = servertText[1];

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
//                            Toast.makeText(mContext, textFromClient + " Pressed", Toast.LENGTH_SHORT).show();


                                if (blockMsg.equalsIgnoreCase("block")) {
                                    Button buzzer = (Button) mConvertView.findViewById(R.id.btnBuzzer);
                                    buzzer.setVisibility(View.GONE);

                                    DeviceListFragment fragment = (DeviceListFragment) fm
                                            .findFragmentById(R.id.fragList);
                                    fragment.updatePressedDevice(teamName, true);


                                } else {
                                    Button buzzer = (Button) mConvertView.findViewById(R.id.btnBuzzer);
                                    buzzer.setVisibility(View.VISIBLE);
                                    DeviceListFragment fragment = (DeviceListFragment) fm
                                            .findFragmentById(R.id.fragList);
                                    fragment.updatePressedDevice(teamName, false);
                                }


                            }
                        });
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (isRunning)
                new Thread(new ReceiveMessage()).start();
        }
    }

    public void sendMessage(String message) {
        try {
            Socket socket = Sockets.getInstance().getClientSocket();

            PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);

            pw.println(message);
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeSocket() {
        isRunning = false;
        try {
            Socket socket = Sockets.getInstance().getClientSocket();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
