package socketconnectons;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by KMac on 8/11/16.
 */
public class Sockets {
    private static final Sockets INSTANCE = new Sockets();
    private ServerSocket mServerSocket;
    private HashMap<String, Socket> hashClientSocket;
    private ArrayList<String> arrClient;
    private Socket clientSocket;

    public Sockets() {
        hashClientSocket = new HashMap<String, Socket>();
        arrClient = new ArrayList<String>();
    }

    public static Sockets getInstance() {

        return INSTANCE;
    }

    public void putServerSocket(ServerSocket mServerSocket) {
        this.mServerSocket = mServerSocket;
    }

    public ServerSocket getServerSocket() {
        return mServerSocket;
    }

    public void putClientSocketInHost(String clientName, Socket clientSocket) {
        hashClientSocket.put(clientName, clientSocket);
        arrClient.add(clientName);
    }

    public Socket getClientSocketInHost(String clientName) {
        return hashClientSocket.get(clientName);

    }

    public ArrayList<String> getArrClient() {
        return arrClient;
    }

    public void putClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

}
