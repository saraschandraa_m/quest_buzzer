package socketconnectons;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.smack.questbuzzer.DeviceListFragment;
import com.smack.questbuzzer.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by KMac on 8/11/16.
 */
public class ServerSocketConnection {
    private Context mContext;
    private View mConvertView;
    private static final ServerSocketConnection INSTANCE = new ServerSocketConnection();
    private FragmentManager fm;
    private boolean mIsPressedTeamReceived = false;
    private boolean isRunning;

    public static ServerSocketConnection getInstance() {
        return INSTANCE;
    }

    public void setIsPressedTeamReceived(boolean mIsPressedTeamReceived) {
        this.mIsPressedTeamReceived = mIsPressedTeamReceived;
    }

    public boolean getIsPressedTeamReceived() {
        return mIsPressedTeamReceived;
    }

    public void initConnection(FragmentManager fm, View mConvertView) {
        this.fm = fm;
        this.mConvertView = mConvertView;
        new Thread(new SocketInitialization()).start();
        isRunning = true;

    }

    public void closeServerSocket() {
        isRunning = false;
        try {
            ServerSocket serverSocket = Sockets.getInstance().getServerSocket();
            if (serverSocket != null)
                serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private class SocketInitialization implements Runnable {
        @Override
        public void run() {
            try {
                ServerSocket serverSocket = new ServerSocket(7777);
                Sockets.getInstance().putServerSocket(serverSocket);
                new Thread(new SocketAccept()).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class SocketAccept implements Runnable {
        @Override
        public void run() {
            try {
                ServerSocket serverSocket = Sockets.getInstance().getServerSocket();
                Socket clientSocket = serverSocket.accept();

                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                String textFromClient = in.readLine();

                Log.d("", "ssuab text from client in server: " + textFromClient);

                Sockets.getInstance().putClientSocketInHost(textFromClient, clientSocket);
                new Thread(new ReceiveMessage(textFromClient)).start();

                new Thread(new SocketAccept()).start();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private class ReceiveMessage implements Runnable {
        String clientName;

        public ReceiveMessage(String clientName) {
            this.clientName = clientName;
        }

        @Override
        public void run() {
            try {
                Socket socket = Sockets.getInstance().getClientSocketInHost(clientName);

                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                final String textFromClient = in.readLine();

                if (textFromClient != null) {

                    Log.d("", "ssuab text from client in server: " + textFromClient);

                    if (!getIsPressedTeamReceived()) {
                        setIsPressedTeamReceived(true);

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
//                            Toast.makeText(mContext, textFromClient + " Pressed", Toast.LENGTH_SHORT).show();

                                DeviceListFragment fragment = (DeviceListFragment) fm
                                        .findFragmentById(R.id.fragList);
                                fragment.updatePressedDevice(clientName, true);


//                            Button buzzerPressed = (Button) mConvertView.findViewById(R.id.btnBuzzerPressed);
//                            buzzerPressed.setVisibility(View.VISIBLE);
//
//                            buzzerPressed.setText(textFromClient);
                                ServerSocketConnection.getInstance().sendMessage("block," + clientName);

                                mConvertView.findViewById(R.id.btnUnBlock).setVisibility(View.VISIBLE);


                            }
                        });
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            if (isRunning)
                new Thread(new ReceiveMessage(clientName)).start();

        }
    }

    public void sendMessage(String message) {
        try {
            Sockets sockets = Sockets.getInstance();
            ArrayList<String> arrClient = sockets.getArrClient();
            int size = arrClient.size();
            for (int i = 0; i < size; i++) {
                Socket socket = sockets.getClientSocketInHost(arrClient.get(i));

                PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);

                pw.println(message);
                pw.flush();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
