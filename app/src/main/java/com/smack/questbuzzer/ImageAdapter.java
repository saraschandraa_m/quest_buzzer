//package com.smack.questbuzzer;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.os.Environment;
//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.util.Base64;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.MediaController;
//import android.widget.VideoView;
//
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//
///**
// * Created by SaraschandraaM on 05/07/16.
// */
//public class ImageAdapter extends PagerAdapter {
//
//    Context mContext;
//    int resource;
//    ArrayList<String> mAttachments;
//    ViewPager pager;
//    AttachmentsHolder holder;
//
//    public ImageAdapter(Context mContext, int resource, ArrayList<String> mAttachments, ViewPager pager) {
//        this.mContext = mContext;
//        this.resource = resource;
//        this.mAttachments = mAttachments;
//        this.pager = pager;
//    }
//
//    @Override
//    public Object instantiateItem(View container, int position) {
//        LayoutInflater inflater = LayoutInflater.from(mContext);
//        View attachmentsView = inflater.inflate(resource, null);
//        holder = new AttachmentsHolder();
//
//        holder.mIvImageShow = (ImageView) attachmentsView.findViewById(R.id.iv_attach_img);
//
//        Uri imagepath = Uri.parse(mAttachments.get(position));
//        holder.mIvImageShow.setImageURI(imagepath);
//
//        ((ViewPager) pager).addView(attachmentsView, 0);
//        return attachmentsView;
//    }
//
//
//    @Override
//    public int getCount() {
//        return mAttachments.size();
//    }
//
//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//        ((ViewPager) container).removeView((View) object);
//    }
//
//    @Override
//    public int getItemPosition(Object object) {
//        return PagerAdapter.POSITION_NONE;
//    }
//
//    @Override
//    public boolean isViewFromObject(View view, Object object) {
//        return view == ((View) object);
//    }
//
//    class AttachmentsHolder {
//        ImageView mIvImageShow;
//    }
//}
