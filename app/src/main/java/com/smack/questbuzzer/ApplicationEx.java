package com.smack.questbuzzer;

import android.app.Application;

/**
 * Created by SaraschandraaM on 04/07/16.
 */
public class ApplicationEx extends Application {


    public static ApplicationEx appInstance;

    public static ApplicationEx getAppInstance() {
        return appInstance;
    }

    public boolean isWifiP2pEnabled;
    public static String TAG = "QuestBuzz";

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
    }

    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }


}
