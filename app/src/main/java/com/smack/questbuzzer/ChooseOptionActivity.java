package com.smack.questbuzzer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by SaraschandraaM on 04/07/16.
 */
public class ChooseOptionActivity extends Activity {

    Button host, join;
    TextView helpText;
    String[] teamArray = new String[]{"A", "B", "C", "D", "E", "F"};                                // For six teams
    boolean isTeamSelected;
    int teamPosition;
    String team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooseoption);
        host = (Button) findViewById(R.id.btn_host);
        join = (Button) findViewById(R.id.btn_join);
        helpText = (TextView) findViewById(R.id.tv_helptext);
        SpannableString content = new SpannableString(getResources().getString(R.string.help));
        content.setSpan(new UnderlineSpan(), 11, 21, 0);
        helpText.setText(content);
        host.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hostIntent = new Intent(ChooseOptionActivity.this, WiFiDirectActivity.class);
                hostIntent.putExtra("type", "host");
                startActivity(hostIntent);
            }
        });
        helpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChooseOptionActivity.this, HelpActivity.class));
            }
        });

        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openContextMenu(view);
            }
        });

        registerForContextMenu(join);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        LayoutInflater headerInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.cell_menuheader, null);

        // menu.setHeaderView(header);
        TextView title = (TextView) header.findViewById(R.id.tv_menuheader);

        if (v.getId() == R.id.btn_join) {
            title.setText("Select Team");
            menu.setHeaderView(header);
            for (int i = 0; i < teamArray.length; i++) {
                menu.add(1, i, Menu.NONE, teamArray[i]);
                menu.setGroupCheckable(1, true, true);
            }

            if (isTeamSelected) {
                MenuItem menuItem = menu.getItem(teamPosition);
                menuItem.setChecked(true);
            }
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();
        String ip = (ipAddress & 0xFF) + "." + ((ipAddress >> 8) & 0xFF) + "." + ((ipAddress >> 16) & 0xFF) + "." + ((ipAddress >> 24) & 0xFF);
        Log.d("", " wifi net , ip: " + ip);


        int position = item.getItemId();
        if (item.getGroupId() == 1) {
            team = teamArray[position];//+"<split>"+ip;
            isTeamSelected = true;
            teamPosition = position;
            Intent joinIntent = new Intent(ChooseOptionActivity.this, WiFiDirectActivity.class);
            joinIntent.putExtra("type", "join");
            joinIntent.putExtra("teamname", team);
            startActivity(joinIntent);
        }

        return super.onContextItemSelected(item);
    }
}
