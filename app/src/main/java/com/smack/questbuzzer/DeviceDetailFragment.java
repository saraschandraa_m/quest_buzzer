/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.smack.questbuzzer;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import socketconnectons.ClientSocketConnection;
import socketconnectons.ServerSocketConnection;

/**
 * A fragment that manages a particular peer and allows interaction with device
 * i.e. setting up network connection and transferring data.
 */
public class DeviceDetailFragment extends Fragment implements ConnectionInfoListener {

    protected static final int CHOOSE_FILE_RESULT_CODE = 20;
    private View mContentView = null;
    private WifiP2pDevice device;
    private WifiP2pInfo info;
    ProgressDialog progressDialog = null;

    ViewPager viewPager;
    CirclePageIndicator circlePageIndicator;
    ArrayList<Bitmap> bitmaps;
    ArrayList<String> images;
    static boolean isServerunset = false;
//    String localhostAddress;


    public static boolean isEntered;
    private boolean isConnected = false;//temp_for_connect_button_visible_probs_after_connect

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContentView = inflater.inflate(R.layout.device_detail, null);


        mContentView.findViewById(R.id.btnConnect).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WifiP2pConfig config = new WifiP2pConfig();
                if (WiFiDirectActivity.getActivityInstance().type.equals("host")) {
                    config.groupOwnerIntent = 15;

                } else {
                    config.groupOwnerIntent = 0;


                }
                config.deviceAddress = device.deviceAddress;
                config.wps.setup = WpsInfo.PBC;
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel",
                        "Connecting to :" + device.deviceAddress, true, true

                );
                ((DeviceListFragment.DeviceActionListener) getActivity()).connect(config);

            }
        });

        mContentView.findViewById(R.id.btnDisConnect).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ((DeviceListFragment.DeviceActionListener) getActivity()).disconnect();
                    }
                });

        mContentView.findViewById(R.id.btnBuzzer).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        MediaPlayer mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.buzzer);
                        mMediaPlayer.start();


                        ClientSocketConnection.getInstance().sendMessage(WiFiDirectActivity.deviceName);

                    }
                });

        mContentView.findViewById(R.id.btnUnBlock).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        DeviceListFragment fragment = (DeviceListFragment) getActivity().getSupportFragmentManager()
                                .findFragmentById(R.id.fragList);
                        fragment.updatePressedDevice("", false);

                        ServerSocketConnection.getInstance().sendMessage("unblock,ds");
                        ServerSocketConnection.getInstance().setIsPressedTeamReceived(false);
                        mContentView.findViewById(R.id.btnUnBlock).setVisibility(View.GONE);
                        mContentView.findViewById(R.id.btnBuzzerPressed).setVisibility(View.GONE);


                    }
                });

//        viewPager = (ViewPager) mContentView.findViewById(R.id.vw_buzzlist);
//        circlePageIndicator = (CirclePageIndicator) mContentView.findViewById(R.id.circlepageindicator);
//        bitmaps = new ArrayList<>();
//        images = new ArrayList<>();
        return mContentView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


    }

    @Override
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.info = info;
        this.getView().setVisibility(View.VISIBLE);

        // The owner IP is now known.
//        TextView view = (TextView) mContentView.findViewById(R.id.group_owner);
//        view.setText(getResources().getString(R.string.group_owner_text)
//                + ((info.isGroupOwner == true) ? getResources().getString(R.string.yes)
//                : getResources().getString(R.string.no)));

        // InetAddress from WifiP2pInfo struct.
//        view = (TextView) mContentView.findViewById(R.id.device_info);
//        view.setText("Group Owner IP - " + info.groupOwnerAddress.getHostAddress());


        // After the group negotiation, we assign the group owner as the file
        // server. The file server is single threaded, single connection server
        // socket.
//        if (WiFiDirectActivity.getActivityInstance().type.equals("host")){//info.groupFormed && info.isGroupOwner) {
//            Log.d("", "ssuab enter into owner");
//
//
//            ServerSocketConnection.getInstance().initConnection(getActivity());
//
//
//            isServerunset = false;
////            localhostAddress = info.groupOwnerAddress.getHostAddress();
//            mContentView.findViewById(R.id.btn_nextquestions).setVisibility(View.VISIBLE);
//            mContentView.findViewById(R.id.vw_buzzlist).setVisibility(View.VISIBLE);
//            mContentView.findViewById(R.id.ll_circlepager).setVisibility(View.VISIBLE);
//
//        } else {//if (info.groupFormed) {
//            Log.d("", "ssuab enter into client");
//
//            ClientSocketConnection.getInstance().initConnection(getActivity(), info.groupOwnerAddress.getHostAddress());
//
//            mContentView.findViewById(R.id.btn_start_client).setVisibility(View.VISIBLE);
//            ((TextView) mContentView.findViewById(R.id.status_text)).setText(getResources()
//                    .getString(R.string.client_text));
//        }


        if (info.groupFormed && info.isGroupOwner) {
            Log.d("", "ssuab enter into owner");


            ServerSocketConnection.getInstance().initConnection(getActivity().getSupportFragmentManager(), mContentView);


            isServerunset = false;
//            localhostAddress = info.groupOwnerAddress.getHostAddress();


            mContentView.findViewById(R.id.btnUnBlock).setVisibility(View.GONE);
            mContentView.findViewById(R.id.btnDisConnect).setVisibility(View.VISIBLE);
            mContentView.findViewById(R.id.btnConnect).setVisibility(View.GONE);
//            mContentView.findViewById(R.id.btnBuzzer).setVisibility(View.VISIBLE);
            mContentView.findViewById(R.id.btnBuzzer).setVisibility(View.GONE);


//            mContentView.findViewById(R.id.btn_nextquestions).setVisibility(View.VISIBLE);
//            mContentView.findViewById(R.id.vw_buzzlist).setVisibility(View.VISIBLE);
//            mContentView.findViewById(R.id.ll_circlepager).setVisibility(View.VISIBLE);

        } else if (info.groupFormed) {
            Log.d("", "ssuab enter into client");


            ClientSocketConnection.getInstance().initConnection(getActivity().getSupportFragmentManager(), info.groupOwnerAddress.getHostAddress(), mContentView);

            mContentView.findViewById(R.id.btnUnBlock).setVisibility(View.GONE);
            mContentView.findViewById(R.id.btnDisConnect).setVisibility(View.GONE);//VISIBLE
            mContentView.findViewById(R.id.btnConnect).setVisibility(View.GONE);
            mContentView.findViewById(R.id.btnBuzzer).setVisibility(View.VISIBLE);
            mContentView.findViewById(R.id.btnBuzzerPressed).setVisibility(View.GONE);
//            ((TextView) mContentView.findViewById(R.id.status_text)).setText(getResources()
//                    .getString(R.string.client_text));
        }


//        mContentView.findViewById(R.id.btn_connect).setVisibility(View.GONE);
//        mContentView.findViewById(R.id.btn_disconnect).setVisibility(View.VISIBLE);
    }

    /**
     * Updates the UI with device data
     *
     * @param device the device to be displayed
     */
    public void showDetails(WifiP2pDevice device, boolean check) {
        this.device = device;
        if (check && !isConnected) {
            isConnected = true;
            mContentView.findViewById(R.id.btnConnect).setVisibility(View.VISIBLE);
        }

//        this.getView().setVisibility(View.VISIBLE);
//        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
//        view.setText(device.deviceAddress);
//        view = (TextView) mContentView.findViewById(R.id.device_info);
//        view.setText(device.toString());

    }

    /**
     * Clears the UI fields after a disconnect or direct mode disable operation.
     */
    public void resetViews() {
//        mContentView.findViewById(R.id.btn_connect).setVisibility(View.VISIBLE);
//        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
//        view.setText(R.string.empty);
//        view = (TextView) mContentView.findViewById(R.id.device_info);
//        view.setText(R.string.empty);
//        view = (TextView) mContentView.findViewById(R.id.group_owner);
//        view.setText(R.string.empty);
//        view = (TextView) mContentView.findViewById(R.id.status_text);
//        view.setText(R.string.empty);
//        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
//        this.getView().setVisibility(View.GONE);
    }

    public void initializeView() {
        if (WiFiDirectActivity.getActivityInstance().type.equalsIgnoreCase("host")) {
            mContentView.findViewById(R.id.btnConnect).setVisibility(View.VISIBLE);
        } else {
            mContentView.findViewById(R.id.btnConnect).setVisibility(View.INVISIBLE);
        }
    }


}
