/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.smack.questbuzzer;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import socketconnectons.ServerSocketConnection;

/**
 * A ListFragment that displays available peers on discovery and requests the
 * parent activity to handle user interaction events
 */
public class DeviceListFragment extends Fragment implements PeerListListener, View.OnClickListener {


    public static List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    ProgressDialog progressDialog = null;
    View mContentView = null;
    private WifiP2pDevice device;
    private WifiP2pDevice clickDevice;
    String names = "";


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        this.setListAdapter(new WiFiPeerListAdapter(getActivity(), R.layout.row_devices, peers));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.device_list, container, false);
        return mContentView;

    }


    /**
     * @return this device
     */
    public WifiP2pDevice getDevice() {
        return device;
    }

    private String getDeviceStatus(int deviceStatus) {
        Log.d(WiFiDirectActivity.TAG, "Peer status :" + deviceStatus);
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";

        }
    }

    /**
     * Initiate a connection with the peer.
     */
//    @Override
//    public void onListItemClick(ListView l, View v, int position, long id) {
////        WifiP2pDevice device = (WifiP2pDevice) getListAdapter().getItem(position);
////        ((DeviceActionListener) getActivity()).showDetails(device);  //click
//    }

    /**
     * Array adapter for ListFragment that maintains WifiP2pDevice list.
     */
//    private class WiFiPeerListAdapter extends ArrayAdapter<WifiP2pDevice> {
//
//        private List<WifiP2pDevice> items;
//
//        /**
//         * @param context
//         * @param textViewResourceId
//         * @param objects
//         */
//        public WiFiPeerListAdapter(Context context, int textViewResourceId,
//                                   List<WifiP2pDevice> objects) {
//            super(context, textViewResourceId, objects);
//            items = objects;
//
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            View v = convertView;
//            if (v == null) {
//                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(
//                        Context.LAYOUT_INFLATER_SERVICE);
//                v = vi.inflate(R.layout.row_devices, null);
//            }
//            WifiP2pDevice device = items.get(position);
//            if (device != null) {
//                TextView top = (TextView) v.findViewById(R.id.device_name);
//                TextView bottom = (TextView) v.findViewById(R.id.device_details);
//                if (top != null) {
//
//                    top.setText(device.deviceName);
//
//                }
//                if (bottom != null) {
//                    bottom.setText(getDeviceStatus(device.status));
//                }
//            }
//
//            return v;
//
//        }
//    }

    /**
     * Update UI for this device.
     *
     * @param device WifiP2pDevice object
     */
    public void updateThisDevice(WifiP2pDevice device) {
        this.device = device;
//        TextView view = (TextView) mContentView.findViewById(R.id.my_name);
//        view.setText(device.deviceName);
//        view = (TextView) mContentView.findViewById(R.id.my_status);
//        view.setText(getDeviceStatus(device.status));

        String name = device.deviceName;

        int colorCode = 0;
        if (device.status == WifiP2pDevice.CONNECTED) {
            colorCode = R.drawable.team_connect;
        } else {
            colorCode = R.drawable.team_available;
        }

        ((DeviceActionListener) getActivity()).showDetails(device, false);
        if (name.equals("Team A")) {
            mContentView.findViewById(R.id.imgTeamA).setVisibility(View.VISIBLE);

            mContentView.findViewById(R.id.imgTeamA).setBackgroundResource(colorCode);


        } else if (name.equals("Team B")) {
            mContentView.findViewById(R.id.imgTeamB).setVisibility(View.VISIBLE);

            mContentView.findViewById(R.id.imgTeamB).setBackgroundResource(colorCode);


        } else if (name.equals("Team C")) {
            mContentView.findViewById(R.id.imgTeamC).setVisibility(View.VISIBLE);

            mContentView.findViewById(R.id.imgTeamC).setBackgroundResource(colorCode);


        } else if (name.equals("Team D")) {
            mContentView.findViewById(R.id.imgTeamD).setVisibility(View.VISIBLE);

            mContentView.findViewById(R.id.imgTeamD).setBackgroundResource(colorCode);


        }else if (name.equals("Team E")) {
            mContentView.findViewById(R.id.imgTeamE).setVisibility(View.VISIBLE);

            mContentView.findViewById(R.id.imgTeamE).setBackgroundResource(colorCode);


        }else if (name.equals("Team F")) {
            mContentView.findViewById(R.id.imgTeamF).setVisibility(View.VISIBLE);

            mContentView.findViewById(R.id.imgTeamF).setBackgroundResource(colorCode);


        } else {
            mContentView.findViewById(R.id.imgMaster).setVisibility(View.VISIBLE);

            mContentView.findViewById(R.id.imgMaster).setBackgroundResource(colorCode);


        }

    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peerList) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        peers.clear();
        peers.addAll(peerList.getDeviceList());
//        ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
        refreshConnectionStatus(peers);
        if (peers.size() == 0) {
            Log.d(WiFiDirectActivity.TAG, "No devices found");
            return;
        }

//        DeviceDetailFragment frag = (DeviceDetailFragment)getFragmentManager().findFragmentById(R.id.frag_detail);
//        frag.showView();


    }

    public void clearPeers() {
        peers.clear();
//        ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
    }

    /**
     *
     */
    public void onInitiateDiscovery() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel", "finding peers", true,
                true, new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
    }

    /**
     * An interface-callback for the activity to listen to fragment interaction
     * events.
     */
    public interface DeviceActionListener {

        void showDetails(WifiP2pDevice device, boolean check);

        void cancelDisconnect();

        void connect(WifiP2pConfig config);

        void disconnect();
    }

    private void refreshConnectionStatus(List<WifiP2pDevice> listPeer) {

//        Toast.makeText(getActivity(), "called", Toast.LENGTH_SHORT).show();


        int size = listPeer.size();
        names = "";
        for (int i = 0; i < size; i++) {

            WifiP2pDevice wifiP2pDevice = listPeer.get(i);
            String name = wifiP2pDevice.deviceName;
            clickDevice = wifiP2pDevice;


            int colorCode = 0;
            if (wifiP2pDevice.status == WifiP2pDevice.CONNECTED) {
                colorCode = R.drawable.team_connect;
                if (names.isEmpty()) {
                    names = name;
                } else {
                    names = names + "," + name;
                }

            } else {
                colorCode = R.drawable.team_available;
            }

            if (name.equals("Team A")) {
                mContentView.findViewById(R.id.imgTeamA).setVisibility(View.VISIBLE);
                mContentView.findViewById(R.id.imgTeamA).setBackgroundResource(colorCode);

//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);

//                mContentView.findViewById(R.id.imgTeamA).setOnClickListener(this);

            } else if (name.equals("Team B")) {
                mContentView.findViewById(R.id.imgTeamB).setVisibility(View.VISIBLE);
                mContentView.findViewById(R.id.imgTeamB).setBackgroundResource(colorCode);
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                mContentView.findViewById(R.id.imgTeamB).setOnClickListener(this);

            } else if (name.equals("Team C")) {
                mContentView.findViewById(R.id.imgTeamC).setVisibility(View.VISIBLE);
                mContentView.findViewById(R.id.imgTeamC).setBackgroundResource(colorCode);
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                mContentView.findViewById(R.id.imgTeamC).setOnClickListener(this);

            } else if (name.equals("Team D")) {
                mContentView.findViewById(R.id.imgTeamD).setVisibility(View.VISIBLE);
                mContentView.findViewById(R.id.imgTeamD).setBackgroundResource(colorCode);
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                mContentView.findViewById(R.id.imgTeamD).setOnClickListener(this);

            } else if (name.equals("Team E")) {
                mContentView.findViewById(R.id.imgTeamE).setVisibility(View.VISIBLE);
                mContentView.findViewById(R.id.imgTeamE).setBackgroundResource(colorCode);
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                mContentView.findViewById(R.id.imgTeamD).setOnClickListener(this);

            }else if (name.equals("Team F")) {
                mContentView.findViewById(R.id.imgTeamF).setVisibility(View.VISIBLE);
                mContentView.findViewById(R.id.imgTeamF).setBackgroundResource(colorCode);
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                mContentView.findViewById(R.id.imgTeamD).setOnClickListener(this);

            }else {
                mContentView.findViewById(R.id.imgMaster).setVisibility(View.VISIBLE);
                mContentView.findViewById(R.id.imgMaster).setBackgroundResource(colorCode);
                ((DeviceActionListener) getActivity()).showDetails(clickDevice, true);
//                mContentView.findViewById(R.id.imgMaster).setOnClickListener(this);

            }


        }
        if (WiFiDirectActivity.type.equalsIgnoreCase("host")) {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {

                    ServerSocketConnection.getInstance().sendMessage("connect<SPLIT>" + names);

                }
            }, 3000);
        }


    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.imgMaster:
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                break;
//            case R.id.imgTeamA:
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                break;
//            case R.id.imgTeamB:
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                break;
//            case R.id.imgTeamC:
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                break;
//            case R.id.imgTeamD:
//                ((DeviceActionListener) getActivity()).showDetails(clickDevice);
//                break;
//        }

    }

    public void updateConnectDevice(String connectDeviceName) {
        String arrNames[] = connectDeviceName.split(",");

//        mContentView.findViewById(R.id.imgTeamA).setBackgroundResource(R.drawable.team_available);
//        mContentView.findViewById(R.id.imgTeamB).setBackgroundResource(R.drawable.team_available);
//        mContentView.findViewById(R.id.imgTeamC).setBackgroundResource(R.drawable.team_available);
//        mContentView.findViewById(R.id.imgTeamD).setBackgroundResource(R.drawable.team_available);

        for (int i = 0; i < arrNames.length; i++) {
            String checkName = arrNames[i];


            if (checkName.equals("Team A")) {

                mContentView.findViewById(R.id.imgTeamA).setBackgroundResource(R.drawable.team_connect);


            } else if (checkName.equals("Team B")) {

                mContentView.findViewById(R.id.imgTeamB).setBackgroundResource(R.drawable.team_connect);


            } else if (checkName.equals("Team C")) {

                mContentView.findViewById(R.id.imgTeamC).setBackgroundResource(R.drawable.team_connect);


            } else if (checkName.equals("Team D")) {

                mContentView.findViewById(R.id.imgTeamD).setBackgroundResource(R.drawable.team_connect);


            }else if (checkName.equals("Team E")) {

                mContentView.findViewById(R.id.imgTeamE).setBackgroundResource(R.drawable.team_connect);


            }else if (checkName.equals("Team F")) {

                mContentView.findViewById(R.id.imgTeamF).setBackgroundResource(R.drawable.team_connect);


            }
        }

    }

    public void updatePressedDevice(String connectDeviceName, boolean isPressed) {
        String arrNames[] = connectDeviceName.split(",");
        for (int i = 0; i < arrNames.length; i++) {
            String checkName = arrNames[i];

            if (isPressed) {
                if (checkName.equals("Team A")) {
                    mContentView.findViewById(R.id.imgTeamA).setBackgroundResource(R.drawable.team_pressed);


                } else if (checkName.equals("Team B")) {

                    mContentView.findViewById(R.id.imgTeamB).setBackgroundResource(R.drawable.team_pressed);


                } else if (checkName.equals("Team C")) {


                    mContentView.findViewById(R.id.imgTeamC).setBackgroundResource(R.drawable.team_pressed);


                } else if (checkName.equals("Team D")) {


                    mContentView.findViewById(R.id.imgTeamD).setBackgroundResource(R.drawable.team_pressed);


                }else if (checkName.equals("Team E")) {


                    mContentView.findViewById(R.id.imgTeamE).setBackgroundResource(R.drawable.team_pressed);


                }else if (checkName.equals("Team F")) {


                    mContentView.findViewById(R.id.imgTeamF).setBackgroundResource(R.drawable.team_pressed);


                }
            } else {
                mContentView.findViewById(R.id.imgTeamA).setBackgroundResource(R.drawable.team_connect);
                mContentView.findViewById(R.id.imgTeamB).setBackgroundResource(R.drawable.team_connect);
                mContentView.findViewById(R.id.imgTeamC).setBackgroundResource(R.drawable.team_connect);
                mContentView.findViewById(R.id.imgTeamD).setBackgroundResource(R.drawable.team_connect);
                mContentView.findViewById(R.id.imgTeamE).setBackgroundResource(R.drawable.team_connect);
                mContentView.findViewById(R.id.imgTeamF).setBackgroundResource(R.drawable.team_connect);
            }
        }

    }
}
