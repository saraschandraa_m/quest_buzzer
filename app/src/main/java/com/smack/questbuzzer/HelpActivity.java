package com.smack.questbuzzer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

/**
 * Created by SaraschandraaM on 09/09/16.
 */
public class HelpActivity extends AppCompatActivity {


    Toolbar mToolbar;
    TabLayout mTabLayout;
    ViewPager mViewPager;

    HelpTabAdapter mHelpTabAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        mToolbar = (Toolbar) findViewById(R.id.tl_tutorial);
        mTabLayout = (TabLayout) findViewById(R.id.tab_tutorial);
        mViewPager = (ViewPager) findViewById(R.id.vw_tutorial);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mHelpTabAdapter = new HelpTabAdapter(this, getSupportFragmentManager());
        mViewPager.setAdapter(mHelpTabAdapter);
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabLayout.setTabTextColors(getResources().getColor(R.color.color_gray), getResources().getColor(R.color.color_white));
        mTabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_white));
        mTabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
