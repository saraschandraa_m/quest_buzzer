package com.smack.questbuzzer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by SaraschandraaM on 10/09/16.
 */
public class TeamFragment extends Fragment {


    View teamView;
    TextView mTvTeamInstructions, mTvSuggestions;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        teamView = inflater.inflate(R.layout.fragment_team, container, false);
        mTvTeamInstructions = (TextView) teamView.findViewById(R.id.tv_teamtext);
        mTvSuggestions = (TextView) teamView.findViewById(R.id.tv_suggestion);
//        SpannableString content = new SpannableString(getResources().getString(R.string.suggestion));
//        content.setSpan(new UnderlineSpan(), 67, 82, 0);
//        mTvSuggestions.setText(content);
        mTvTeamInstructions.setText(getResources().getString(R.string.instructions_team));
        return teamView;
    }
}
