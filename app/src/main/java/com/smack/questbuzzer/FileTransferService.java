// Copyright 2011 Google Inc. All Rights Reserved.

package com.smack.questbuzzer;

import android.app.Activity;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * A service that process each file transfer request i.e Intent by opening a
 * socket connection with the WiFi Direct Group Owner and writing the file
 */
public class FileTransferService extends IntentService {

    private static final int SOCKET_TIMEOUT = 60000;
    public static final String ACTION_SEND_FILE = "com.example.android.wifidirect.SEND_FILE";
    public static final String EXTRAS_FILE_PATH = "file_url";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "go_port";

    private Socket socket;

    public FileTransferService(String name) {
        super(name);
    }

    public FileTransferService() {
        super("FileTransferService");
    }

    /*
     * (non-Javadoc)
     * @see android.app.IntentService#onHandleIntent(android.content.Intent)
     */
    @Override
    protected void onHandleIntent(Intent intent) {


        Context context = getApplicationContext();
        if (intent.getAction().equals(ACTION_SEND_FILE)) {
            String fileUri = intent.getExtras().getString(EXTRAS_FILE_PATH);
            String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            socket = null;// = new Socket();
            int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);

            try {

                socket = SocketClasses.getSocketClassesInstance().getSocketInstance();
//
                if(socket==null) {
                Log.d(WiFiDirectActivity.TAG, "Opening client socket - ");
                socket = new Socket();
                socket.bind(null);
//                if(!DeviceDetailFragment.isGroup)
                socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);
//                else
//                    socket.connect((new InetSocketAddress(InetAddress.getByName(DeviceDetailFragment.listIp.get(0)), 8988)), SOCKET_TIMEOUT);//DeviceDetailFragment.listIp.get(0)
                    SocketClasses.getSocketClassesInstance().putSocketInstance(socket);
                }
//                Log.d("","ssuab sending socket ip: "+host);
//
//                Log.d(WiFiDirectActivity.TAG, "Client socket - " + socket.isConnected());
//                OutputStream stream = socket.getOutputStream();
//                ContentResolver cr = context.getContentResolver();
//                InputStream is = null;
//                try {
//                    is = cr.openInputStream(Uri.parse(fileUri));
//                } catch (FileNotFoundException e) {
//                    Log.d(WiFiDirectActivity.TAG, e.toString());
//                }


//                DeviceDetailFragment.copyFile(is, stream);


                PrintWriter pw =  new PrintWriter(socket.getOutputStream(),true);

               pw.println("Testing ssuab");
                pw.flush();

                if(!DeviceDetailFragment.isEntered){
                    DeviceDetailFragment.isEntered = true;
                  new Thread(new serverThread()).start();

                }

                MediaPlayer mMediaPlayer = MediaPlayer.create(context, R.raw.buzzer);
                mMediaPlayer.start();
                Log.d(WiFiDirectActivity.TAG, "Client: Data written");
            } catch (IOException e) {
                Log.e(WiFiDirectActivity.TAG, e.getMessage());
            } finally {
//                if (socket != null) {
//                    if (socket.isConnected()) {
//                        try {
//                            socket.close();
//                        } catch (IOException e) {
//                            // Give up
//                            e.printStackTrace();
//                        }
//                    }
//                }
            }

        }
    }

     class serverThread implements Runnable{
        @Override
        public void run() {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                final String textFromClient = in.readLine(); // read the text from client
                Log.d("", "ssuab text from server: " + textFromClient);
                if(textFromClient!=null){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),textFromClient,Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                new Thread(new serverThread()).start();

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
