package com.smack.questbuzzer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by SaraschandraaM on 10/09/16.
 */
public class HostFragment extends Fragment {

    View hostView;

    TextView mTvHostInstructions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        hostView = inflater.inflate(R.layout.fragment_host, container, false);
        mTvHostInstructions = (TextView) hostView.findViewById(R.id.tv_hosttext);

        mTvHostInstructions.setText(getActivity().getResources().getString(R.string.instructions_host));
        return hostView;
    }
}
