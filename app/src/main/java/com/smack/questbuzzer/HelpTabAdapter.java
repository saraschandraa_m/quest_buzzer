package com.smack.questbuzzer;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by SaraschandraaM on 10/09/16.
 */
public class HelpTabAdapter extends FragmentStatePagerAdapter {

    Context mContext;
    String[] title = new String[]{"Quiz Master", "Team"};

    public HelpTabAdapter(Context mContext, FragmentManager fm) {
        super(fm);
        this.mContext = mContext;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HostFragment();
                break;

            case 1:
                fragment = new TeamFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
