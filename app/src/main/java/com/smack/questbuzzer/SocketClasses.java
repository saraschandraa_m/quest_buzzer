package com.smack.questbuzzer;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by KMac on 8/1/16.
 */
public class SocketClasses {
    private static SocketClasses socketClasses=  new SocketClasses();
    private Socket socket;
    private ServerSocket serverSocket;
    private Socket socketClient;

    private ServerSocket serverSocketClient;

    public static SocketClasses getSocketClassesInstance(){
        return socketClasses;
    }
    public Socket getSocketInstance(){
        return socket;
    }
    public void putSocketInstance(Socket socket){
        this.socket= socket;
    }
    public ServerSocket getServerSocket(){
        return serverSocket;
    }
    public void putServerSocket(ServerSocket serverSocket){
        this.serverSocket = serverSocket;
    }
    public void putServerSocketClient(ServerSocket serverSocketClient){
        this.serverSocketClient = serverSocketClient;

    }
    public ServerSocket getServerSocketClient(){
        return this.serverSocketClient;
    }
    public void putSocketClient(Socket socketClient){
        this.socketClient = socketClient;
    }
    public Socket getSocketClient(){
        return socketClient;
    }
}
