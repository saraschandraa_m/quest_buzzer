package services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.smack.questbuzzer.WiFiDirectActivity;

/**
 * Created by KMac on 8/26/16.
 */
public class ForceQuiteService extends Service{
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        WiFiDirectActivity.getActivityInstance().disconnect();
        stopSelf();
    }
}
